package com.test.junit.mockito.junit.mockito.test.component;

import com.test.junit.mockito.junit.mockito.test.entity.PersonEntity;
import com.test.junit.mockito.junit.mockito.test.model.PersonModel;
import com.test.junit.mockito.junit.mockito.test.properties.ApplicationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UtilComponent {
  
  @Autowired
  private ApplicationProperties applicationProperties;
  
  public PersonModel mapper(PersonEntity personEntity) {
    PersonModel personModel = new PersonModel();
    
    personModel.setPersonId(personEntity.getPersonId());
    personModel.setName(personEntity.getName());
    personModel.setLastName(personEntity.getLastName());
    personModel.setDate(personEntity.getBirthDate());
    personModel.setStatus(getStatus(personEntity.getStatus()));
    
    return personModel;
  }
  
  private String getStatus(String statusCode) {
    String status = "";
    switch (statusCode) {
      case "00":
        status = applicationProperties.getPostulanteReason();
        break;
      case "01":
        status = applicationProperties.getInscritoReason();
        break;
      case "02":
        status = applicationProperties.getEnCursoReason();
        break;
      case "03":
        status = applicationProperties.getBajaTemporalReason();
        break;
      case "04":
        status = applicationProperties.getBajaDefinitivaReason();
        break;
      default:
        status = applicationProperties.getDefaultReason();
        break;
    }
    return status;
  }
  
}
